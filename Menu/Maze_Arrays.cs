﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Menu
{
    class Maze_Arrays
    {
        public static string[,] Arr()
        {
            Random random = new Random();
            int value = random.Next(1, 21);


            string[,] grid1 =
            {
                {" ", "|", "|", "|", "|"},
                {" ", "|", " ", "|", "X"},
                {" ", "|", " ", " ", " "},
                {" ", " ", " ", "|", "|"},
                {" ", " ", " ", " ", "|"},
                {"|", "|", "|", " ", "|"}
            };

            string[,] grid2 =
            {
                {" ", " ", " ", "|", "|"," ", "|", "|", "|", "|"},
                {" ", "|", " ", "|", " ", "|", "|", "|", "|", "X"},
                {" ", "|", " ", " ", "|", "|", "|", "|", " ", " "},
                {" ", " ", "|", " ", "|", "|", "|", "|", "|", " "},
                {" ", "|", "|", " ", " "," ", "|", " ", " ", " "},
                {"|", "|", " ", " ", "|", " ", " "," ", "|", " "},
                {"|", "|", " ", " ", "|", " ", " "," ", "|", " "},
                {" ", " ", " ", "|", "|"," ", "|", "|", "|", "|"}
            };

            string[,] grid3 =
            {
                {" ", "|", " ", "|", "|"," ", "|", "|", "|", "|"},
                {" ", " ", " ", "|", " ", "|", "|", " ", " ", "|"},
                {"|", "|", " ", " ", "|", "|", "|", " ", " ", " "},
                {" ", " ", "|", " ", "|", "|", "|", " ", "|", " "},
                {" ", "|", " ", " ", "|"," ", " ", " ", "|", " "},
                {"|", "|", " ", " ", "|", " ", " "," ", "|", " "},
                {"|", "|", "|", " ", " ", " ", "|","|", " ", "X"},
                {" ", " ", " ", "|", "|"," ", "|", "|", "|", "|"},
                {"|", "|", "|", "|", "|"," ", "|", "|", "|", "|"}
            };
            string[,] grid4 =
           {
                {" ", "|", "|", "|"},
                {" ", "|", " ", "X"},
                {" ", " ", " ", " "},
                {" ", " ", "|", "|"}
            };

            string[,] grid5 =
            {
                {" ", "|", "|"},
                {" ", " ", " "},
                {"|", "|", " "},
                {"|", "X", " "}
            };

            string[,] grid6 =
           {
                {" ", "|", " ", "|", "|"," ", " ", " ", "|", "|"},
                {" ", " ", " ", " ", " ", " ", "|", " ", " ", "|"},
                {"|", "|", "|", "|", "|", "|", "|", "|", " ", " "},
                {" ", " ", "|", " ", "|", "|", "|", " ", "|", " "},
                {" ", "|", " ", " ", "|"," ", " ", "|", " ", " "},
                {"|", "|", " ", " ", "|", " ", " "," ", "|", " "},
                {"|", "|", " ", "|", " ", " ", "|","|", " ", " "},
                {" ", " ", " ", "|", " "," ", "|", "|", "|", "|"},
                {"|", "|", "|", "X", " ","|", "|", "|", "|", "|"}
            };
            string[,] grid7 =
            {
                {" ", " ", "|", "|", "|", "|", "|", "|", "|", "|"},
                {"|", " ", "|", "|", " ", " ", " ", " ", " ", "|"},
                {"|", " ", " ", " ", " ", " ", " ", "|", " ", "|"},
                {"|", "|", "|", "|", " ", "|", " ", "|", " ", "|"},
                {"|", " ", " ", " ", " ", "|", " ", "|", " ", "|"},
                {"|", "|", " ", "|", "|", "|", " ", "|", " ", "|"},
                {"|", "|", " ", " ", " ", " ", " ", "|", " ", "|"},
                {"|", "|", "|", "|", "|", "|", "|", "|", " ", "|"},
                {"|", " ", " ", " ", " ", " ", " ", " ", " ", "|"},
                {"|", "|", "|", "|", "|", "|", "X", "|", "|", "|"}
            };
            string[,] grid8 =
            {
                { " ","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"," ","|","|","|","|"},
                { " "," ","|","|"," "," "," "," "," ","|"," "," "," "," "," ","|","|","|","|"," "," "," ","|","|"},
                { "|"," ","|","|"," ","|","|","|","|","|"," ","|","|","|"," "," "," ","|","|","|","|"," ","|","|"},
                { "|"," ","|","|"," ","|","|","|"," "," "," ","|","|","|","|","|"," "," "," ","|","|"," ","|","|"},
                { " "," ","|","|"," ","|","|","|"," ","|","|","|"," "," "," ","|","|","|"," "," "," "," ","|","|"},
                { "|"," "," "," "," "," "," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," ","|","|"},
                { "|"," ","|"," "," ","|"," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," "," ","X"},
                { "|"," ","|"," "," ","|"," ","|"," ","|"," "," "," ","|"," "," "," ","|","|","|","|","|"," ","|"},
                { "|"," ","|"," "," ","|"," ","|"," ","|"," ","|"," ","|","|","|"," ","|","|","|","|"," "," ","|"},
                { "|"," ","|","|","|","|"," ","|"," ","|"," ","|"," ","|","|","|"," "," "," "," ","|"," ","|","|"},
                { "|"," "," ","|","|","|"," ","|"," ","|"," ","|"," ","|","|","|"," ","|","|"," ","|"," ","|","|"},
                { "|"," "," "," ","|","|"," ","|"," ","|"," ","|"," "," "," "," "," ","|","|"," "," "," ","|","|"},
                { "|"," ","|"," "," "," "," "," "," ","|"," ","|","|"," ","|","|","|","|","|","|","|","|","|","|"},
                { "|"," "," ","|","|","|","|","|"," "," "," ","|","|"," ","|","|","|","|","|","|","|","|","|","|"},
                { "|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"}
            };
            string[,] grid9 =
            {
                    { " ","|","|","|","|","|","|","|","|","|","|"},
                    { " "," ","|","|","|","|","|","|","|","|","|"},
                    { "|"," ","|","|","|","|","|","|","|","|","|"},
                    { "|"," "," "," "," "," "," "," "," ","|","|"},
                    { "|"," ","|"," ","|","|","|","|"," "," "," "},
                    { "|"," "," "," ","|","|","|","|","|","|"," "},
                    { "|","|"," "," ","|"," "," "," ","|","|"," "},
                    { "|","|","|"," ","|"," ","|"," ","|","|"," "},
                    { "|","|","|"," "," "," ","|"," "," ","X"," "},
                    { "|","|","|","|","|","|","|","|","|","|","|"},
            };

            string[,] grid10 =
            {
                {" ","|","|","|","|","|","|","|"},
                {" "," "," "," ","|","|","|","|"},
                {"|"," ","|"," "," "," ","|","|"},
                {"|"," ","|"," "," "," "," ","|"},
                {"|"," ","|","|","|","|"," ","|"},
                {"|"," "," ","|","|","|"," "," "},
                {"|","|"," ","|","|","|","|"," "},
                {"|","|"," "," "," "," "," ","X"},
                {"|","|","|","|","|","|","|","|"},
            };
            string[,] grid11 =
            {
                {" "," ","|","|","|","|","|","|"},
                {"|"," ","|"," "," "," ","|","|"},
                {" "," "," "," ","|"," "," "," "},
                {"|"," ","|","|","|"," ","|","|"},
                {"|"," ","|","|","|"," "," ","|"},
                {"|"," ","|","|","|","|"," ","|"},
                {"|"," ","|","|","|","|"," "," "},
                {"|"," ","|"," ","|"," "," "," "},
                {"|"," ","|"," ","|"," "," "," "},
                {"|"," ","|"," "," "," ","|"," "},
                {"|"," ","|"," ","|","|","|"," "},
                {"|"," "," "," ","|","|","|"," "},
                {"|","|","|"," "," "," "," "," "},
                {"|","|","|","|","|","|","X","|"},
            };
            string[,] grid12 =
            {
                {" ", " ", "|", "|", "|", "|", "|", "|"},
                {"|", " ", "|", " ", " ", " ", "|", "|"},
                {"|", " ", "|", " ", "|", " ", " ", " "},
                {"|", " ", " ", " ", "|", "|", "|", " "},
                {"|", "|", "|", " ", "|", "|", " ", " "},
                {"|", " ", " ", " ", "|", " ", " ", "|"},
                {"|", "|", "|", "|", " ", " ", " ", "|"},
                {"|", " ", " ", " ", " ", "|", " ", "|"},
                {"|", "|", "X", "|", "|", "|", " ", "|"},
            };
            string[,] grid13 =
            {
                    { " ","|","|","|","|","|"},
                    { " ","|"," "," ","|","|"},
                    { " ","|"," "," "," "," "},
                    { " "," "," ","|","|"," "},
                    { "|"," ","|","|"," "," "},
                    { "|"," "," ","|"," ","|"},
                    { "|","|"," ","|"," ","|"},
                    { " "," "," ","|"," ","X"},
                    { "|","|","|","|","|","|"},
            };
            string[,] grid14 =
            {
                { " ", " ", "|", "|", "|", "|", "|", "|"},
                { "|", " ", "|", " ", " ", " ", "|", "|"},
                { "|", " ", "|", " ", "|", " ", "|", "|"},
                { "|", " ", "|", " ", "|", " ", "|", "|"},
                { "|", " ", "|", " ", "|", " ", "|", "|"},
                { "|", " ", "|", " ", "|", " ", "|", "|"},
                { "|", " ", "|", " ", "|", " ", "|", "|"},
                { "|", " ", " ", " ", "|", " ", " ", "|"},
                { "|", "|", "|", "|", "|", "|", "X", "|"},
            };
            string[,] grid15 =
           {
                { " ", " ", "|", "|", "|", "|", "|", "|"},
                { "|", " ", " ", "|", " ", " ", " ", "|"},
                { "|", "|", " ", "|", " ", "|", " ", "|"},
                { " ", " ", " ", "|", " ", "|", " ", " "},
                { " ", "|", " ", " ", " ", "|", " ", "|"},
                { "|", "|", "|", "|", " ", "|", " ", "|"},
                { "|", " ", " ", " ", " ", "|", " ", "|"},
                { "|", "|", " ", "|", " ", " ", " ", "|"},
                { "|", "|", "|", "|", "|", "|", "X", "|"},
            };
            string[,] grid16 =
            {
                { " ", " ", "|", "|", "|", "|", "|", "|", "|"},
                { "|", " ", " ", " ", " ", " ", " ", " ", "|"},
                { "|", "|", "|", "|", "|", " ", "|", " ", "|"},
                { "|", " ", " ", " ", "|", " ", "|", " ", "|"},
                { "|", " ", "|", " ", "|", " ", "|", " ", "|"},
                { "|", " ", "|", " ", " ", " ", " ", " ", "|"},
                { "|", " ", "|", " ", "|", "|", "|", " ", "|"},
                { "|", " ", " ", " ", " ", " ", "|", " ", "|"},
                { "|", "|", "|", "X", "|", "|", "|", "|", "|"}
            };
            string[,] grid17 =
            {
                { " ", " ", "|", "|", "|", "|", "|", "|", "|"},
                { " ", " ", " ", " ", " ", " ", " ", " ", "|"},
                { "|", " ", "|", "|", "|", " ", "|", " ", "|"},
                { "|", " ", " ", " ", "|", " ", "|", " ", "|"},
                { "|", " ", "|", " ", "|", " ", "|", " ", "|"},
                { "|", "|", "|", " ", " ", " ", "|", " ", "|"},
                { "|", " ", "|", "|", "|", "|", "|", " ", "|"},
                { "|", " ", " ", " ", " ", " ", " ", " ", "|"},
                { "|", " ", "|", "|", "|", " ", "|", "|", "|"},
                { "|", " ", " ", " ", "|", " ", " ", "|", "|"},
                { "|", "|", "|", "|", "|", "X", "|", "|", "|"}
            };
            string[,] grid18 =
            {
                { " ","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"},
                { " "," "," "," "," ","|","|","|","|","|"," "," "," "," "," ","|","|","|","|","|","|","|","|","|"},
                { "|"," ","|","|"," "," "," "," "," ","|"," ","|","|","|"," "," "," ","|","|","|","|","|","|","|"},
                { "|"," ","|","|","|","|","|","|"," "," "," ","|","|","|","|","|"," "," "," ","|","|","|","|","|"},
                { "|"," ","|","|","|","|","|","|"," ","|","|","|"," "," "," ","|","|","|"," "," "," "," ","|","|"},
                { "|"," "," "," "," "," "," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," ","|","|"},
                { "|"," ","|"," "," ","|"," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," "," ","X"},
                { "|"," ","|"," "," ","|"," ","|"," ","|"," "," "," ","|"," "," "," ","|","|","|","|","|"," "," "},
                { "|"," ","|"," "," ","|"," ","|"," ","|"," ","|","|","|","|","|"," ","|","|","|","|"," "," "," "},
                { "|"," ","|","|","|","|"," ","|"," ","|"," ","|","|","|","|","|"," "," "," "," ","|"," ","|"," "},
                { "|"," "," ","|","|","|"," ","|"," ","|"," ","|","|","|","|","|","|","|","|"," ","|"," ","|"," "},
                { "|","|"," "," ","|","|"," ","|"," ","|"," ","|","|"," "," "," "," ","|","|"," "," "," ","|"," "},
                { "|","|","|"," "," "," "," "," "," ","|"," ","|","|"," ","|","|"," ","|","|","|","|"," "," "," "},
                { "|","|","|","|","|","|","|","|"," "," "," "," "," "," ","|","|"," "," "," "," "," "," ","|","|"},
                { "|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"}
            };
            string[,] grid19 =
            {
                { " "," ","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"},
                { "|"," "," "," "," ","|","|","|","|","|"," "," "," "," "," ","|","|","|","|","|","|","|","|","|"},
                { "|"," ","|","|"," "," "," "," "," ","|"," ","|","|","|"," "," "," ","|","|","|","|","|","|","|"},
                { "|"," ","|","|","|","|","|","|"," "," "," ","|","|","|","|","|"," "," "," ","|","|","|","|","|"},
                { "|"," ","|","|","|","|","|","|"," ","|","|","|"," "," "," ","|","|","|"," "," "," "," ","|","|"},
                { "|"," "," "," "," "," "," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," ","|","|"},
                { "|"," ","|"," "," ","|"," ","|"," ","|","|","|"," ","|"," ","|","|","|","|","|","|"," "," ","|"},
                { "|"," ","|"," "," ","|"," ","|"," ","|"," "," "," ","|"," "," "," ","|","|","|","|","|"," "," "},
                { "|","|","|"," "," ","|"," ","|"," ","|"," ","|","|","|","|","|"," ","|","|","|","|"," "," "," "},
                { "|","|","|","|","|","|"," ","|"," ","|"," ","|","|","|","|","|"," "," "," "," ","|"," ","|"," "},
                { "|","|"," ","|","|","|"," ","|"," ","|"," ","|","|","|","|","|","|","|","|"," ","|"," ","|"," "},
                { "|","|"," "," ","|","|"," ","|"," ","|"," ","|","|"," "," "," "," ","|","|"," "," "," ","|"," "},
                { "|","|"," "," "," "," "," "," "," ","|"," ","|","|"," ","|"," "," ","|","|","|","|"," "," "," "},
                { " "," "," ","|","|","|"," ","|"," "," "," "," "," "," ","|"," "," "," "," "," "," "," ","|","|"},
                { " ","|","|","|","|","|"," ","|","|","|","|","|","|","|","|"," ","|","|","|","|","|"," ","|","|"},
                { " ","|","|","|","|","|"," ","|","|","|","|","|","|","|","|"," ","|","|","|","|","|"," ","|","|"},
                { " ","|","|","|","|","|"," ","|","|","|","|","|","|","|","|"," ","|","|","|","|","|"," ","|","|"},
                { " ","|","|","|"," "," "," ","|","|","|","|","|","|","|","|"," "," "," "," ","X"," "," ","|","|"},
                { " "," "," ","|"," ","|"," "," ","|"," "," "," "," "," ","|","|","|","|","|"," ","|","|","|","|"},
                { "|","|"," ","|"," ","|","|"," "," "," ","|","|","|"," ","|","|","|","|","|"," ","|","|","|","|"},
                { "|","|"," "," "," ","|","|","|","|","|","|","|","|"," "," "," "," "," "," "," ","|","|","|","|"},
                { "|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|","|"}
            };
            string[,] grid20 =
            {
                {" ", " ", "|", "|", "|", "|", "|", "|", "|", "|", " ", "|"},
                {"|", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "|"},
                {"|", "|", "|", " ", "|", "|", " ", "|", "|", "|", "|", "|"},
                {"|", " ", "|", " ", "|", " ", " ", "|", " ", " ", " ", "|"},
                {"|", " ", "|", " ", "|", "|", " ", "|", " ", "|", " ", "|"},
                {"|", " ", " ", " ", " ", " ", " ", "|", " ", "|", " ", "|"},
                {"|", " ", "|", "|", "|", "|", "|", "|", " ", "|", " ", "|"},
                {"|", " ", " ", " ", " ", " ", " ", "|", " ", "|", " ", "|"},
                {"|", " ", "|", "|", "|", "|", " ", "|", " ", "|", " ", "|"},
                {"|", " ", "|", " ", "|", "|", " ", " ", " ", "|", " ", "|"},
                {"|", " ", "|", " ", "|", "|", " ", "|", " ", "|", " ", "|"},
                {"|", " ", " ", " ", "|", " ", " ", " ", " ", "|", " ", "|"},
                {"|", "|", "|", "|", "|", "|", "|", "|", "|", "|", "X", "|"},
            };
            switch (value)
            {
                case 1: return grid1;
                case 2: return grid2;
                case 3: return grid3;
                case 4: return grid4;
                case 5: return grid5;
                case 6: return grid6;
                case 7: return grid7;
                case 8: return grid8;
                case 9: return grid9;
                case 10: return grid10;
                case 11: return grid11;
                case 12: return grid12;
                case 13: return grid13;
                case 14: return grid14;
                case 15: return grid15;
                case 16: return grid16;
                case 17: return grid17;
                case 18: return grid18;
                case 19: return grid19;
                case 20: return grid20;
                default: return grid1;

            }
        }
    }
}
